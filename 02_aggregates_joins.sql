select
    scheduled_departure
from flights
where scheduled_departure between '2017-08-01 00:00:00' and '2017-08-31 23:59:59'
order by scheduled_departure desc;

select
    count(*) > 0 as strange_flights
from flights
where departure_airport IN ('DME', 'VKO')
    and arrival_airport IN ('DME', 'VKO');

select distinct
    --DATE(scheduled_arrival) as scheduled_arrival_date
    --scheduled_arrival::date as scheduled_arrival_date,
    CAST(scheduled_arrival AS DATE) as scheduled_arrival_date
from flights
order by DATE(scheduled_arrival);

select
    count(distinct scheduled_arrival::DATE)
from flights;

select
    count(actual_departure) as cnt
from flights;

select
    min(scheduled_arrival) as min_scheduled_arrival,
    max(scheduled_arrival) as max_scheduled_arrival
from flights;

select
    status,
    count(*) as cnt
from flights
group by status;

select
    scheduled_departure::date,
    status,
    count(*) as cnt
from flights
group by scheduled_departure::date, status
order by scheduled_departure::date, status;

select --distinct
    scheduled_departure::date,
    status
from flights
group by scheduled_departure::date, status
order by scheduled_departure::date, status;

select
    scheduled_departure::date,
    status,
    count(*) - count(actual_departure) as cnt
from flights
group by scheduled_departure::date, status
order by scheduled_departure::date, status;

select
    *
from flights f, aircrafts a
where f.aircraft_code = a.aircraft_code;

select
    flight_id,
    flight_no,
    a.aircraft_code,
    model
from flights f
left join aircrafts a on f.aircraft_code = a.aircraft_code;

select
    count(*) cnt
from flights f
left join aircrafts a on f.aircraft_code = a.aircraft_code;

select
    count(*) cnt
from aircrafts a
left join flights f on f.aircraft_code = a.aircraft_code;

select
    *
from aircrafts a
left join flights f on f.aircraft_code = a.aircraft_code
where flight_id is null;

select
    *
from flights f
right join aircrafts a on f.aircraft_code = a.aircraft_code
where flight_id is null;

select
    *
from flights f
left join aircrafts a on f.aircraft_code = a.aircraft_code
where flight_id is null;

select
    *
from flights f
inner join aircrafts a on f.aircraft_code = a.aircraft_code
where flight_id is null;

select
    *
from flights f
full outer join aircrafts a on f.aircraft_code = a.aircraft_code
where flight_id is null;
