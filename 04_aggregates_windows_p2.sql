-- 1. Вывести пассажиров {passenger_id, passenger_name}, путешествовавшие только региональными рейсами (не должно быть полётов в Москву и из Москвы). Решить с использованием EXISTS и NOT EXISTS

with ticket_flights_extended as (
    select
        tf.ticket_no,
        a1.city as departure_city,
        a2.city as arrival_city
    from ticket_flights tf
    left join flights f on tf.flight_id = f.flight_id
    left join airports a1 on f.departure_airport = a1.airport_code
    left join airports a2 on f.arrival_airport = a2.airport_code
    where a1.city = 'Moscow' or a2.city = 'Moscow'
)

select distinct
    passenger_id,
    passenger_name
from tickets t
where not exists(
    select
        *
    from ticket_flights_extended tf
    where t.ticket_no = tf.ticket_no
);

-- 2. Аналогично предыдущему, решить без использования EXISTS и NOT EXISTS.

with
     moscow_passengers AS (
         select distinct
            passenger_id
         from tickets t
         left join ticket_flights tf on t.ticket_no = tf.ticket_no
         left join flights f on tf.flight_id = f.flight_id
         left join airports a1 on f.departure_airport = a1.airport_code
         left join airports a2 on f.arrival_airport = a2.airport_code
         where a1.city = 'Moscow' or a2.city = 'Moscow'
     )

select distinct
    t.passenger_id,
    t.passenger_name
from tickets t
left join moscow_passengers m on t.passenger_id = m.passenger_id
where m.passenger_id is null;

-- Оконные функции - вывести рейсы, для каждого вывести:
-- - кол-во рейсов, которые были в воздухе меньше текущего или столько же;
-- - кол-во рейсов, которые были в воздухе столько же, сколько и текущий;
-- - кол-во рейсов, которые были в воздухе строго меньше текущего
-- - долю рейсов, которые были в воздухе меньше текущего или столько же, сколько и текущий, относительно всех рейсов;

select
    *,
    count(*) over (order by actual_arrival - actual_departure) less_equal_duration_count,
    count(*) over (partition by actual_arrival - actual_departure) equal_duration_count,
    count(*) over (order by actual_arrival - actual_departure) - count(*) over (partition by actual_arrival - actual_departure) less_duration_count,
    count(*) over (order by actual_arrival - actual_departure) * 100.0 / count(*) over () less_equal_duration_percent
from flights
where actual_departure is not null and actual_arrival is not null
order by actual_arrival - actual_departure;
