SELECT
    *
FROM flights;

SELECT
    flight_no,
    aircraft_code
FROM flights;

SELECT
    *
FROM flights
WHERE arrival_airport = 'UFA'
ORDER BY scheduled_departure
LIMIT 5;

SELECT
    *
FROM flights
WHERE 1=1
    AND arrival_airport = 'UFA'
    AND status <> 'Arrived'
ORDER BY scheduled_departure
LIMIT 5;

SELECT
    *
FROM flights
WHERE 1=1
    AND arrival_airport = 'UFA'
    AND status NOT IN ('Arrived', 'On Time')
ORDER BY scheduled_departure;

SELECT DISTINCT
    status
FROM flights
WHERE 1=1
    AND arrival_airport = 'UFA'
    AND status NOT IN ('Arrived', 'On Time');

SELECT DISTINCT
    departure_airport,
    status
FROM flights
WHERE 1=1
    AND arrival_airport = 'UFA'
    AND status NOT IN ('Arrived', 'On Time');

SELECT
    *
FROM tickets
WHERE passenger_name LIKE 'ARTUR %';

SELECT
    *
FROM tickets
WHERE passenger_name LIKE 'ARTUR ______';

SELECT
    *
FROM flights
WHERE scheduled_departure BETWEEN DATE('2017-08-01') AND '2017-08-02';

SELECT
    COUNT(*) cnt
FROM flights
WHERE scheduled_departure BETWEEN DATE('2017-08-01') AND '2017-08-02';

SELECT
    COUNT(*) cnt,
    MIN(scheduled_departure) min_dep,
    MAX(scheduled_departure) max_dep
FROM flights
WHERE scheduled_departure BETWEEN DATE('2017-08-01') AND '2017-08-02';

SELECT
    COUNT(DISTINCT passenger_name) AS cnt
FROM tickets;

SELECT
    *,
    CASE
        WHEN status = 'Arrived' THEN 'Ok'
        ELSE 'Not ok'
    END AS transformed_status
FROM flights;

SELECT
    *
FROM flights
WHERE actual_departure IS NOT NULL;

